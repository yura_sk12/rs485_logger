﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using System.IO;

namespace rs485_logger
{
    public partial class COM_settings : Form
    {
        SerialPort serialPort1 = null;
        public string port_name, baud_rate, parity, data_bits, stop_bits;
        public COM_settings(SerialPort port)
        {
            InitializeComponent();
            serialPort1 = port;
        }

        bool check_connection()
        {
            setting_button();
            bool status = status_port();
            set_status(Convert.ToInt16(status));
            set_status(Convert.ToInt16(status));
            return status;
        }

        public bool status_port()
        {
            return serialPort1.IsOpen;
        }

        /// <summary>
        /// Выолняет инициализацию combobox
        /// </summary>
        private void cb_init()
        {
            set_available_port();
            cb_baudrate.SelectedIndex = 3;
            cb_parity.SelectedIndex = 0;
            cb_databits.SelectedIndex = 3;
            cb_stopbits.SelectedIndex = 1;
        }

        /// <summary>
        /// Заполняет combobox cb_com всеми доступныи портами
        /// </summary>
        private void set_available_port()
        {
            cb_com.Items.Clear();
            string[] availabe_ports = this.get_available_ports();
            cb_com.Items.AddRange(availabe_ports);
            select_true_port(availabe_ports);
        }

        /// <summary>
        /// Функция для выбора подходащего порта автоматически
        /// </summary>
        private void select_true_port(string[] ports)
        {
            if (ports.Length == 1)
            {
                cb_com.SelectedIndex = 0;
            }
        }

        public SerialPort serialPort
        {
            get
            {
                return serialPort1;
            }
        }

        /// <summary>
        /// Возвращает список доступных COM портов
        /// </summary>
        /// <returns>Список доступных портов</returns>
        private string[] get_available_ports()
        {
            return SerialPort.GetPortNames();
        }

        /// <summary>
        /// Закрывает соединение с портом
        /// </summary>
        private void close_connection()
        {
            try
            {
                serialPort1.Close();
            }
            catch (System.IO.IOException e)
            {
                MessageBox.Show("Ошибка ввода вывода при закрытии " + e.Message);
                
            }
        }

        private void save_setting_to_file()
        {
            // Перезапись файла
            using (StreamWriter sw = new StreamWriter(Form1.file_path, false, System.Text.Encoding.Default))
            {
                sw.Write("PortName:" + serialPort1.PortName);
                sw.WriteLine();

                sw.Write("BaudRate:" + serialPort1.BaudRate);
                sw.WriteLine();

                sw.Write("Parity:" + serialPort1.Parity);
                sw.WriteLine();

                sw.Write("DataBits:" + serialPort1.DataBits);
                sw.WriteLine();

                sw.Write("StopBits:" + serialPort1.StopBits);
                sw.WriteLine();
            }
        }

        /// <summary>
        /// Выполняет подключение к порту
        /// </summary>
        /// <returns>True - Подключение выполенено</returns>
        /// <returns>True - Ошибка подключения</returns>
        public bool connect(SerialPort serialPort)
        {
            try
            {
                init_serial_port(serialPort1);
                serialPort.Open();
            }
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show("Ошибка доступа к порту \r\n" + e.Message);
                set_status(2);
                return false;
            }
            catch (ArgumentOutOfRangeException e)
            {
                MessageBox.Show("Выход за пределы допустимых диапазонов \r\n" + e.Message);
                set_status(2);
                return false;
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Ошибка аргумента \r\n" + e.Message);
                set_status(2);
                return false;
            }
            catch (System.IO.IOException e)
            {
                MessageBox.Show("Ошибка ввода вывода \r\n" + e.Message);
                set_status(2);
                return false;
            }
            set_status(1);

            return true;
        }

        private void _load_param_from_cb()
        {
            this.port_name = cb_com.SelectedItem.ToString();
            this.baud_rate = cb_baudrate.SelectedItem.ToString();
            this.parity = cb_parity.SelectedItem.ToString();
            this.data_bits = cb_databits.SelectedItem.ToString();
            this.stop_bits = cb_stopbits.SelectedItem.ToString();
        }

        /// <summary>
        /// Инициализация serialPort
        /// </summary>
        public void init_serial_port(SerialPort serialPort)
        {
            serialPort.PortName = this.port_name;
            serialPort.BaudRate = Convert.ToInt16(this.baud_rate);
            switch (this.parity.ToLower())
            {
                case "none":
                    serialPort.Parity = Parity.None;
                    break;
                case "even":
                    serialPort.Parity = Parity.Even;
                    break;
                case "mark":
                    serialPort.Parity = Parity.Mark;
                    break;
                case "odd":
                    serialPort.Parity = Parity.Odd;
                    break;
                case "space":
                    serialPort.Parity = Parity.Space;
                    break;
                default:
                    MessageBox.Show("Внимание не верно задан parity");
                    serialPort.Parity = Parity.None;
                    break;
            }
            serialPort.DataBits = Convert.ToInt16(this.data_bits);

            switch (this.stop_bits.ToLower())
            {
                case "none":
                    serialPort.StopBits = StopBits.None;
                    break;
                case "one":
                    serialPort.StopBits = StopBits.One;
                    break;
                case "onePointFive":
                    serialPort.StopBits = StopBits.OnePointFive;
                    break;
                case "two":
                    serialPort.StopBits = StopBits.Two;
                    break;
                default:
                    MessageBox.Show("Внимание не верно задан stopbits");
                    serialPort.StopBits = StopBits.One;
                    break;
            }
        }

        /// <summary>
        /// Отключение от порта
        /// </summary>
        private void disconnect_from_port()
        {
            try
            {
                serialPort1.Close();
            } catch (System.IO.IOException e)
            {
                MessageBox.Show("Не возможно закрыть соединение \r\n" + e.Message);
            }
        }

        private void bt_connect_Click(object sender, EventArgs e)
        {
            if (status_port())
            {
                disconnect_from_port();
            } else
            {
                _load_param_from_cb();
                if (connect(serialPort1))
                {
                    save_setting_to_file();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            check_connection();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            set_available_port();
        }

        /// <summary>
        /// Выполняет визуальные настройки статуса;
        /// 0 - Disconnected, 1 - Сonnected, 2 - error
        /// </summary>
        /// <param name="status"></param>
        private void set_status(int status)
        {
            switch (status)
            {
                case 0:
                    lb_status.Text = "Disconnected";
                    lb_status.BackColor = Color.Pink;
                    break;
                case 1:
                    lb_status.Text = "Connected";
                    lb_status.BackColor = Color.Green;
                    break;
                case 2:
                    lb_status.Text = "Error";
                    lb_status.BackColor = Color.Red;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Настройка кнопки, когда есть соединение кнопка для отключения
        /// Если соединения нету то кнопка используется для подключения
        /// </summary>
        private bool setting_button()
        {
            if (status_port())
            {
                bt_connect.Text = "Отключиться";
                return true;
            } else
            {
                bt_connect.Text = "Подключиться";
                return false;
            }
        }

        /// <summary>
        /// Настрока окна при его показе
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void COM_settings_Shown(object sender, EventArgs e)
        {
            cb_init();
            check_connection();
        }
    }
}
