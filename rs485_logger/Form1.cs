﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using System.IO;

namespace rs485_logger
{
    public partial class Form1 : Form
    {
        public static string file_path = "settings.txt";
        private SerialPort serialPort = new SerialPort();
        public Form1()
        {
            InitializeComponent();
            if (!auto_connect())
            {
                MessageBox.Show("Соединение не успешно, проверте настроки");
            }
        }

        /// <summary>
        /// Автоматически подключется по параметрам с предыдущего включения
        /// Параметры храняться в file_path
        /// </summary>
        private bool auto_connect()
        {
            COM_settings com_settings_form = new COM_settings(serialPort);
            FileInfo fileInf = new FileInfo(file_path);
            if (fileInf.Exists)
            {
                using (StreamReader sr = new StreamReader(file_path, System.Text.Encoding.Default))
                {
                    string PortName = sr.ReadLine().Split(':')[1];
                    string BaudRate = sr.ReadLine().Split(':')[1];
                    string Parity = sr.ReadLine().Split(':')[1];
                    string DataBits = sr.ReadLine().Split(':')[1];
                    string StopBits = sr.ReadLine().Split(':')[1];

                    com_settings_form.port_name = PortName;
                    com_settings_form.baud_rate= BaudRate;
                    com_settings_form.parity = Parity;
                    com_settings_form.data_bits = DataBits;
                    com_settings_form.stop_bits = StopBits;

                    return com_settings_form.connect(serialPort);
                }
            }
            return false;
        }

        private void COMportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            COM_settings com_settings_form = new COM_settings(serialPort);
            com_settings_form.ShowDialog();
            if (com_settings_form.DialogResult != DialogResult.OK)
                return;
        }

        private void отладкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Debug_serial_form debug_serial_form = new Debug_serial_form(serialPort);
            debug_serial_form.Show();
            if (debug_serial_form.DialogResult != DialogResult.OK)
                return;
        }

        
    }
}
