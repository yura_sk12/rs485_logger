﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace rs485_logger
{
    class WorkSerial
    {
        SerialPort serialPort1 = null;
        private byte ID = 0x01;
        private short END = 0x5555;
        private byte COMMAND = 0x11;
        private UInt16 CRC = 0;

        public WorkSerial(SerialPort serialPort)
        {
            serialPort1 = serialPort;
        }

        public byte RS485_ID { get => ID; set => ID = value; }
        public short RS485_END { get => END; set => END = value; }
        public byte RS485_COMMAND { get => COMMAND; set => COMMAND = value; }
        public ushort RS485_CRC { get => CRC; set => CRC = value; }

        /// <summary>
        /// Формирует массив байт для отправки
        /// </summary>
        /// <returns></returns>
        private byte[] get_buffer_to_send()
        {
            byte[] b_end = BitConverter.GetBytes(END);
            byte[] without_crc = new byte[] { ID, COMMAND,};
            CRC = ModRTU_CRC(without_crc, without_crc.Length);
            byte[] b_crc = BitConverter.GetBytes(CRC);
            byte[] with_crc = new byte[] { ID, COMMAND, b_crc[0], b_crc[1] };
            return with_crc;
        }

        /// <summary>
        /// Отправляет команду на приемник
        /// </summary>
        /// <param name="command"></param>
        public void send_command(byte command)
        {
            COMMAND = command;
            serialPort1.Write(get_buffer_to_send(), 0, get_buffer_to_send().Length);
        }

        /// <summary>
        /// Вычисляет CRC код переданного массива byte
        /// </summary>
        /// <returns></returns>
        public static UInt16 ModRTU_CRC(byte[] buf, int len)
        {
            UInt16 crc = 0xFFFF;
            for (int pos = 0; pos < len; pos++)
            {
                crc ^= (UInt16)buf[pos];
                for (int i = 8; i != 0; i--)
                {
                    if ((crc & 0x0001) != 0)
                    {
                        crc >>= 1;
                        crc ^= 0xA001;
                    }
                    else
                        crc >>= 1;
                }
            }
            // Помните, что младший и старший байты поменяны местами, используйте соответственно (или переворачивайте)
            return crc;
        }

        /// <summary>
        /// Вычисляет CRC код переданного массива int
        /// </summary>
        /// <returns></returns>
        public static UInt16 ModRTU_CRC(int[] buf, int len)
        {
            UInt16 crc = 0xFFFF;
            for (int pos = 0; pos < len; pos++)
            {
                byte tmp = (byte)buf[pos];
                crc ^= (UInt16)tmp;
                for (int i = 8; i != 0; i--)
                {
                    if ((crc & 0x0001) != 0)
                    {
                        crc >>= 1;
                        crc ^= 0xA001;
                    }
                    else
                        crc >>= 1;
                }
            }
            // Помните, что младший и старший байты поменяны местами, используйте соответственно (или переворачивайте)
            return crc;
        }
    }
}
