﻿namespace rs485_logger
{
    partial class Debug_serial_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_out = new System.Windows.Forms.TextBox();
            this.lb_status = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_end = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_command = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_id = new System.Windows.Forms.TextBox();
            this.bt_send = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_crc = new System.Windows.Forms.TextBox();
            this.rb_hex = new System.Windows.Forms.RadioButton();
            this.rb_bin = new System.Windows.Forms.RadioButton();
            this.rb_string = new System.Windows.Forms.RadioButton();
            this.lb_devices = new System.Windows.Forms.ListBox();
            this.bt_clear = new System.Windows.Forms.Button();
            this.cb_command = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_out
            // 
            this.tb_out.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_out.Location = new System.Drawing.Point(12, 141);
            this.tb_out.Multiline = true;
            this.tb_out.Name = "tb_out";
            this.tb_out.Size = new System.Drawing.Size(581, 433);
            this.tb_out.TabIndex = 0;
            // 
            // lb_status
            // 
            this.lb_status.AutoSize = true;
            this.lb_status.Location = new System.Drawing.Point(122, 9);
            this.lb_status.Name = "lb_status";
            this.lb_status.Size = new System.Drawing.Size(0, 13);
            this.lb_status.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Статус соединения";
            // 
            // tb_end
            // 
            this.tb_end.Location = new System.Drawing.Point(722, 12);
            this.tb_end.Name = "tb_end";
            this.tb_end.Size = new System.Drawing.Size(100, 20);
            this.tb_end.TabIndex = 5;
            this.tb_end.Text = "21845";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(599, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Символ конца строки";
            // 
            // tb_command
            // 
            this.tb_command.Location = new System.Drawing.Point(722, 38);
            this.tb_command.Name = "tb_command";
            this.tb_command.Size = new System.Drawing.Size(100, 20);
            this.tb_command.TabIndex = 7;
            this.tb_command.Text = "17";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(599, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Команда";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(599, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "ID";
            // 
            // tb_id
            // 
            this.tb_id.Location = new System.Drawing.Point(722, 64);
            this.tb_id.Name = "tb_id";
            this.tb_id.Size = new System.Drawing.Size(100, 20);
            this.tb_id.TabIndex = 10;
            this.tb_id.Text = "1";
            // 
            // bt_send
            // 
            this.bt_send.Location = new System.Drawing.Point(722, 90);
            this.bt_send.Name = "bt_send";
            this.bt_send.Size = new System.Drawing.Size(100, 23);
            this.bt_send.TabIndex = 11;
            this.bt_send.Text = "Отправить";
            this.bt_send.UseVisualStyleBackColor = true;
            this.bt_send.Click += new System.EventHandler(this.bt_send_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(393, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Расчитанный CRC";
            // 
            // tb_crc
            // 
            this.tb_crc.Location = new System.Drawing.Point(493, 12);
            this.tb_crc.Name = "tb_crc";
            this.tb_crc.Size = new System.Drawing.Size(100, 20);
            this.tb_crc.TabIndex = 12;
            // 
            // rb_hex
            // 
            this.rb_hex.AutoSize = true;
            this.rb_hex.Location = new System.Drawing.Point(12, 118);
            this.rb_hex.Name = "rb_hex";
            this.rb_hex.Size = new System.Drawing.Size(92, 17);
            this.rb_hex.TabIndex = 16;
            this.rb_hex.TabStop = true;
            this.rb_hex.Text = "Вывод в HEX";
            this.rb_hex.UseVisualStyleBackColor = true;
            // 
            // rb_bin
            // 
            this.rb_bin.AutoSize = true;
            this.rb_bin.Location = new System.Drawing.Point(111, 118);
            this.rb_bin.Name = "rb_bin";
            this.rb_bin.Size = new System.Drawing.Size(88, 17);
            this.rb_bin.TabIndex = 17;
            this.rb_bin.TabStop = true;
            this.rb_bin.Text = "Вывод в BIN";
            this.rb_bin.UseVisualStyleBackColor = true;
            // 
            // rb_string
            // 
            this.rb_string.AutoSize = true;
            this.rb_string.Checked = true;
            this.rb_string.Location = new System.Drawing.Point(206, 118);
            this.rb_string.Name = "rb_string";
            this.rb_string.Size = new System.Drawing.Size(102, 17);
            this.rb_string.TabIndex = 18;
            this.rb_string.TabStop = true;
            this.rb_string.Text = "Вывод строкой";
            this.rb_string.UseVisualStyleBackColor = true;
            // 
            // lb_devices
            // 
            this.lb_devices.FormattingEnabled = true;
            this.lb_devices.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lb_devices.Location = new System.Drawing.Point(602, 141);
            this.lb_devices.Name = "lb_devices";
            this.lb_devices.Size = new System.Drawing.Size(221, 433);
            this.lb_devices.TabIndex = 19;
            // 
            // bt_clear
            // 
            this.bt_clear.Location = new System.Drawing.Point(602, 90);
            this.bt_clear.Name = "bt_clear";
            this.bt_clear.Size = new System.Drawing.Size(100, 23);
            this.bt_clear.TabIndex = 20;
            this.bt_clear.Text = "Очистить";
            this.bt_clear.UseVisualStyleBackColor = true;
            this.bt_clear.Click += new System.EventHandler(this.bt_clear_Click);
            // 
            // cb_command
            // 
            this.cb_command.FormattingEnabled = true;
            this.cb_command.Location = new System.Drawing.Point(472, 37);
            this.cb_command.Name = "cb_command";
            this.cb_command.Size = new System.Drawing.Size(121, 21);
            this.cb_command.TabIndex = 21;
            this.cb_command.SelectedIndexChanged += new System.EventHandler(this.cb_command_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(393, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Команды";
            // 
            // Debug_serial_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 580);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cb_command);
            this.Controls.Add(this.bt_clear);
            this.Controls.Add(this.lb_devices);
            this.Controls.Add(this.rb_string);
            this.Controls.Add(this.rb_bin);
            this.Controls.Add(this.rb_hex);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_crc);
            this.Controls.Add(this.bt_send);
            this.Controls.Add(this.tb_id);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_command);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_end);
            this.Controls.Add(this.lb_status);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_out);
            this.Name = "Debug_serial_form";
            this.Text = "debug_serial_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_out;
        private System.Windows.Forms.Label lb_status;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_end;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_command;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_id;
        private System.Windows.Forms.Button bt_send;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_crc;
        private System.Windows.Forms.RadioButton rb_hex;
        private System.Windows.Forms.RadioButton rb_bin;
        private System.Windows.Forms.RadioButton rb_string;
        private System.Windows.Forms.ListBox lb_devices;
        private System.Windows.Forms.Button bt_clear;
        private System.Windows.Forms.ComboBox cb_command;
        private System.Windows.Forms.Label label6;
    }
}