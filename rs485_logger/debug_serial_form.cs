﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using System.Collections.Generic;


namespace rs485_logger
{
    public partial class Debug_serial_form : Form
    {
        SerialPort serialPort1 = null;
        WorkSerial workSerial = null;
        Queue<byte> recive_buff = new Queue<byte>();
        string[] cmd_arr = { "len", "err" };

        Dictionary<string, int> modbus_func_dict = new Dictionary<string, int>();

        enum modbus_func_enum
        {
            FUNC_LENGTH,  // Длинна полотна
            FUNC_ANS,     // Используется для ответа Хранит код ошибки
            FUNC_SET_REG, // Функция установки регистра
            FUNC_CLEAR_LENGTH,  // Очистка регистра длинны
            FUNC_GET_ANS,   // Отправка регистра с последней ошибкой
        };

        bool _read_continue = true;
        bool _parse_continue = true;
        public Debug_serial_form(SerialPort serialPort)
        {
            modbus_func_dict.Add("FUNC_LENGTH",         0);
            modbus_func_dict.Add("FUNC_ANS",            1);
            modbus_func_dict.Add("FUNC_SET_REG",        2);
            modbus_func_dict.Add("FUNC_CLEAR_LENGTH",   3);
            modbus_func_dict.Add("FUNC_GET_ANS",        4);

            InitializeComponent();
            serialPort1 = serialPort;
            cb_command.Items.AddRange(modbus_func_dict.Keys.ToArray());
            cb_command.SelectedIndex = 0;
            check_connection();
            workSerial = new WorkSerial(serialPort1);
            Thread readThread = new Thread(Read);
            Thread parseThread = new Thread(Parse);
            readThread.Start();
            parseThread.Start();
        }

        private void check_connection()
        {
            if (!serialPort1.IsOpen)
            {
                set_status(0);
            }
            else
            {
                set_status(1);
            }
        }

        /// <summary>
        /// Выполняет визуальные настройки статуса;
        /// 0 - Disconnected, 1 - Сonnected, 2 - error
        /// </summary>
        /// <param name="status"></param>
        private void set_status(int status)
        {
            switch (status)
            {
                case 0:
                    lb_status.Text = "Disconnected";
                    lb_status.BackColor = Color.Pink;
                    break;
                case 1:
                    lb_status.Text = "Connected";
                    lb_status.BackColor = Color.Green;
                    break;
                case 2:
                    lb_status.Text = "Error";
                    lb_status.BackColor = Color.Red;
                    break;
                default:
                    break;
            }
        }

        private bool __parse_reg_crc(Queue<byte> q, out int reg, byte id, modbus_func_enum func)
        {
            wait_data(q, 6);
            byte reg_1 = q.Dequeue();
            byte reg_2 = q.Dequeue();
            byte reg_3 = q.Dequeue();
            byte reg_4 = q.Dequeue();
            reg = reg_1 | (reg_2 << 8) | (reg_3 << 16) | (reg_4 << 24);

            byte crc_1 = q.Dequeue();
            byte crc_2 = q.Dequeue();
            int crc = crc_1 | (crc_2 << 8);
            byte[] arr = { id, (byte)func, reg_1, reg_2, reg_3, reg_4 };
            UInt16 calc_crc = WorkSerial.ModRTU_CRC(arr, arr.Length);

            if (crc == calc_crc)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        // Обработчик потока парсера recive_buff
        private void Parse()
        {
            while (_parse_continue)
            {
                // если find_id выполнилась то Id найден
                find_id();
                wait_data(recive_buff, 1);
                byte id = byte.Parse(tb_id.Text);
                switch ((int)recive_buff.Dequeue())
                {
                    case (int)modbus_func_enum.FUNC_LENGTH:
                        int len = 0;
                        if (__parse_reg_crc(recive_buff, out len, id, modbus_func_enum.FUNC_LENGTH))
                        {
                            BeginInvoke(new MethodInvoker(delegate
                            {
                                add_to_lb_devices("Длинна с устр: " + id + " = " + len);
                            }));
                        }

                        break;

                    case (int)modbus_func_enum.FUNC_ANS:
                        int ans = 0;
                        if (__parse_reg_crc(recive_buff, out ans, id, modbus_func_enum.FUNC_ANS))
                        {
                            BeginInvoke(new MethodInvoker(delegate
                            {
                                add_to_lb_devices("Ответ с устр: " + id + " = " + ans);
                            }));
                        }
                        break;
                }
            }
            int a = 0;
        }

        private void find_id()
        {
            while (true)
            {
                while (recive_buff.Count > 0)
                {
                    byte id = byte.Parse(tb_id.Text);
                    if (recive_buff.Dequeue() == id)
                        return;
                }
            }
        }

        private bool wait_data(Queue<byte> q, int cnt)
        {
            while (q.Count < cnt)
            {
            }
            return true;
        }

        // Остановить thread при выходе из программы
        private void Read()
        {
            while (_read_continue)
            {
                try
                {
                    if (serialPort1.BytesToRead > 0)
                    {
                        int cnt = serialPort1.BytesToRead;
                        byte[] arr = new byte[cnt];
                        for (int i = 0; i < cnt; i++)
                        {
                            arr[i] = (byte)serialPort1.ReadByte();
                        }
                        for (int i = 0; i < cnt; i++)
                        {
                            recive_buff.Enqueue((byte)arr[i]);
                        }
                        BeginInvoke(new MethodInvoker(delegate
                        {
                            append_to_textView(arr);
                        }));
                    }
                }
                catch (TimeoutException) { }
            }
        }

        /// <summary>
        /// Конвертирует переданные данные в строку битов
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string convert_to_bin(string str)
        {
            string ret = "";
            byte[] asciiBytes = Encoding.ASCII.GetBytes(str);
            foreach (byte item in asciiBytes)
            {
                ret += Convert.ToString(item, 2) + "_";
            }
            return ret;
        }

        /// <summary>
        /// Конвертирует переданные данные в строку битов
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string convert_to_bin(int[] str)
        {
            string ret = "";
            foreach (byte item in str)
            {
                ret += Convert.ToString(item, 2) + "_";
            }
            return ret;
        }

        /// <summary>
        /// Конвертирует переданные данные в строку битов
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string convert_to_bin(byte[] str)
        {
            string ret = "";
            foreach (byte item in str)
            {
                ret += Convert.ToString(item, 2) + "_";
            }
            return ret;
        }

        /// <summary>
        /// Конвертирует переданные данные в строку hex
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string convert_to_hex(string str)
        {
            string ret = "";
            byte[] asciiBytes = Encoding.ASCII.GetBytes(str);
            foreach (byte item in asciiBytes)
            {
                ret += Convert.ToString(item, 16) + "_";
            }
            return ret;
        }

        /// <summary>
        /// Конвертирует переданные данные в строку hex
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string convert_to_hex(int[] str)
        {
            string ret = "";
            foreach (byte item in str)
            {
                ret += Convert.ToString(item, 16) + "_";
            }
            return ret;
        }

        /// <summary>
        /// Конвертирует переданные данные в строку hex
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string convert_to_hex(byte[] str)
        {
            string ret = "";
            foreach (byte item in str)
            {
                ret += Convert.ToString(item, 16) + "_";
            }
            return ret;
        }

        private void add_to_lb_devices(string str)
        {
            lb_devices.Items.Add(str);
            lb_devices.SelectedIndex = lb_devices.Items.Count - 1;
        }

        private void append_to_textView(byte [] bytes)
        {
            if (rb_bin.Checked)
            {
                tb_out.AppendText(convert_to_bin(bytes) + "\r\n");
            }
            else if (rb_hex.Checked)
            {
                tb_out.AppendText(convert_to_hex(bytes) + "\r\n");
            } else if (rb_string.Checked)
            {
                string tmp = "";
                foreach (char ch in bytes)
                {
                    if (ch == '\n')
                    {
                        tmp += Environment.NewLine;
                    } else if (ch == '\0')
                    {
                        tmp += "\\0";
                    } else
                    {
                        tmp += ch;
                    }
                    /*if (char.IsLetter(ch) || check_in_word(ch))
                    {
                        tmp += ch;
                    } else if (ch == '\n')
                    {
                        tmp += Environment.NewLine;
                    } else if (char.IsNumber(ch))
                    {
                        tmp += '_' + Char.GetNumericValue(ch).ToString();
                    } else if (char.IsSeparator(ch))
                    {
                        tmp += ch;
                    } else
                    {
                        tmp += '_' + Convert.ToString(Convert.ToByte(ch), 10);
                    }*/
                }
                tb_out.AppendText(tmp);
            }
        }

        private bool check_in_word(char ch)
        {
            char[] arr = { ':', ';', ',', '_', '_', ' ' };
            foreach (char item in arr)
            {
                if (item == ch)
                    return true;
            }
            return false;
        }

        private void setting_work_serial()
        {
            short end = 0;
            byte id = 0;
            byte command = 0;
            
            bool res = short.TryParse(tb_end.Text, out end);
            if (res)
                workSerial.RS485_END = end;

            res = Byte.TryParse(tb_id.Text, out id);
            if (res)
                workSerial.RS485_ID = id;
            
            res = Byte.TryParse(tb_command.Text, out command);
            if (res)
                workSerial.RS485_COMMAND = command;
        }

        private void bt_send_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                setting_work_serial();
                workSerial.send_command(workSerial.RS485_COMMAND);
                tb_crc.Text = workSerial.RS485_CRC.ToString();
            }
        }

        private void bt_clear_Click(object sender, EventArgs e)
        {
            tb_out.Clear();
            lb_devices.Items.Clear();
        }

        private void cb_command_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_command.Text = modbus_func_dict[cb_command.SelectedItem.ToString()].ToString();
        }
    }
}
